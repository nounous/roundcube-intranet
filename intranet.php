<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Roundcube. If not, see http://www.gnu.org/licenses/.
 */
class intranet extends rcube_plugin
{

	function init()
	{
		$this->load_config();

		$this->add_texts('localization', false);
		$this->include_stylesheet($this->local_skin_path() .'/intranet.css');

		$rcmail = rcmail::get_instance();

		$button = array(
			'type' => 'link',
			'class' => 'button-intranet',
			'content' => '<span class="button-inner">'.$rcmail->config->get('intranet_name').'</span>',
			'href'=>$rcmail->config->get('intranet_url'),
		);
		$this->add_button($button, 'taskbar');
	}

}

?>
